'use strict';
// 1 ЧАСТЬ
class Pokemon {
	constructor(name, level = 0, foundStatus = true) {
		this.name = name;
		this.level = level;
		this.foundStatus = foundStatus;
	}
	show(){
		console.log(`Меня зовут ${this.name}, мой уровень - ${this.level}.`);
	}
};

let pickachu = new Pokemon("Pikachu", 80, false);
let ekans = new Pokemon("Ekans", 44, false);
let vuplix = new Pokemon("Vulpix", 22, true);
let squirtle = new Pokemon("Squirtle", 36, true);

class PokemonList extends Array {
	add(name, level = 0, foundStatus = true){
		this.push(new Pokemon(name, level, foundStatus));
	}
	show(){
		this.forEach((pokemon)=>{
			console.log(`Имя покемона: ${pokemon.name}. Уровень: ${pokemon.level}. Найден: ${pokemon.foundStatus}`);
		})
		console.log(`Общее количество покемонов в списке = ${this.length}`);
	}
	max(){
		let allLevels = this.map(pokemon => pokemon.level);
		let maxLevel = Math.max(...allLevels);
		let indexPokemon = allLevels.findIndex(level => level == maxLevel);
		let pokemon = this[indexPokemon];
		return pokemon
	}
};
const pokemons = new PokemonList(pickachu, ekans, vuplix, squirtle);
pokemons.add("Nidoran", 44, true);

// 2 ЧАСТЬ
const found = pokemons.filter(pokemon => pokemon.foundStatus);
const lost = pokemons.filter(pokemon => pokemon.foundStatus != true);
found.add("Zubat", 76, true);
lost.add("Gloom", 4, false);
found.show();
lost.show();

const transferPokemon = (array1, array2, name, status) => {
	let pokemonIndex = array1.findIndex(pokemon => pokemon.name == name);
	let pokemon = array1.splice(pokemonIndex,1);
	pokemon[0].foundStatus = status;
	array2.push(pokemon[0]);
}

transferPokemon(lost, found, "Gloom", true);
found.show();
lost.show();

let stengthPokemon = lost.max();
console.log(stengthPokemon);

// Не понял как именно можно переопределить метод valueOf для решения этой задачи 
// Поэтому предложил свой метод решения) Не судите строго)
// Спасибо за подсказку на счет статусов во 2 лекции.


